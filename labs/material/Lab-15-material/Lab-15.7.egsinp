##############################################################################
### GEOMETRY
##############################################################################

:start geometry definition:
    
    
    #-------------------------------------------------------------------------
    # spherical chamber tip
    #-------------------------------------------------------------------------
    :start geometry:
        name     = chamber_tip
        library  = egs_spheres
        midpoint = 0.935 0 -10
        radii = 0.05 0.305 0.355            
        :start media input:
            media = PMMA521ICRU AIR521ICRU
            set medium = 1 1
        :stop media input:
    :stop geometry:


    #-------------------------------------------------------------------------
    # main chamber body (as a conestack)                         
    #-------------------------------------------------------------------------
    :start geometry:
        name = chamber_body
        library = egs_cones
        type = EGS_ConeStack
        axis = 0.985 0 -10 -1 0 0
        
        ### sensitive volume (cylindrical portion)
        :start layer:
            thickness    = 2.03
            top radii    = 0.05 0.305 0.355
            bottom radii = 0.05 0.305 0.355
            media        = PMMA521ICRU AIR521ICRU PMMA521ICRU
        :stop layer:

        ### electrode base
        :start layer:
            thickness    = 0.13
            top radii    = 0.15 0.305 0.355
            bottom radii = 0.15 0.305 0.355
            media        = PMMA521ICRU AIR521ICRU PMMA521ICRU
        :stop layer:

        ### up to first kink
        :start layer:
            thickness    = 0.11
            top radii    = 0.355
            bottom radii = 0.355
            media        = PMMA521ICRU
        :stop layer:
                    
        ### first widening
        :start layer:
            thickness    = 0.08
            bottom radii = 0.48
            media        = PMMA521ICRU
        :stop layer:
        
        ### up to second kink
        :start layer:
            thickness    = 0.48
            bottom radii = 0.48
            media        = PMMA521ICRU
        :stop layer:
                    
        ### second widening
        :start layer:
            thickness    = 0.08
            bottom radii = 0.61
            media        = PMMA521ICRU
        :stop layer:
                    
        ### to end
        :start layer:
            thickness    = 2.0
            bottom radii = 0.61
            media        = PMMA521ICRU
        :stop layer:

    :stop geometry:   

        
    #-------------------------------------------------------------------------
    # join tip and body in cd geometry
    #-------------------------------------------------------------------------
    :start geometry:
        name = cd_planes_for_chamber
        library = egs_planes
        type = EGS_Xplanes
        positions = -4 0.935 4
    :stop geometry:

    :start geometry:
        name = chamber
        library = egs_cdgeometry
        base geometry = cd_planes_for_chamber
        set geometry = 0 chamber_body
        set geometry = 1 chamber_tip
        new indexing style = 1
    :stop geometry:
    
    
    #-------------------------------------------------------------------------
    # define water phantom with air above it
    #-------------------------------------------------------------------------
    
    ### long column of water
    :start geometry:
        name     = water_column
        library  = egs_box
        box size = 30 30 1000
        :start media input:
            media = H2O521ICRU
        :stop media input:
    :stop geometry:
    
    ### long column of air
    :start geometry:
        name     = air_column
        library  = egs_box
        box size = 30 30 1000
        :start media input:
            media = AIR521ICRU
        :stop media input:
    :stop geometry:
    
    ### define base planes for cd geometry
    :start geometry:
        name    = cd_planes_for_phantom
        library = egs_planes
        type    = EGS_Zplanes
        positions = -30 0 110
    :stop geometry:
    
    ### put everything togheter using a cd geometry
    :start geometry:
        name    = phantom
        library = egs_cdgeometry
        base geometry = cd_planes_for_phantom
        set geometry  = 0 water_column
        set geometry  = 1 air_column
    :stop geometry:          
    
    
    #-------------------------------------------------------------------------
    # inscribe chamber in phantom with an envelope
    #------------------------------------------------------------------------- 
    :start geometry:
        name    = chamber@10cm
        library = egs_genvelope
        base geometry = phantom
        inscribed geometries = chamber
    :stop geometry:   
        
    
    #-------------------------------------------------------------------------
    # set simulation geometry
    #-------------------------------------------------------------------------      
    simulation geometry = chamber@10cm
    
:stop geometry definition:




##############################################################################
### SOURCE
##############################################################################

:start source definition:

    :start source:
        name      = my_source           
        library   = egs_collimated_source            
        charge    = 0                  
        
        ### point source 100 cm away on +z axis
        :start source shape:                   
            type     = point        
            position = 0 0 100
        :stop source shape:
        
        ### collimation to 10x10 field at phantom surface
        :start target shape:             
            library   = egs_rectangle   
            rectangle = -5 0 5 5
        :stop target shape:
        
        ### monoenergetic spectrum
        :start spectrum:
            type = monoenergetic
            energy = 6 MeV
        :stop spectrum:
    :stop source:
    
    simulation source = my_source

:stop source definition:



##############################################################################
### SCORING OPTIONS
##############################################################################
:start scoring options:

    :start calculation geometry:
        geometry name   = chamber@10cm 
        cavity regions  = 3 6 24           # all regions for air cavity
        cavity mass     = 1                # cavity mass in g/cm^3
        cavity geometry = chamber@10cm     # (for range rejection)
    :stop calculation geometry:

:stop scoring options:


##############################################################################
### MONTE CARLO PARAMETERS
##############################################################################
:start MC transport parameter:                                                                                                                                        
    Global Ecut = 0.521        # cutoff energy for electrons
    Global Pcut = 0.01         # cutoff energy for photons
:stop MC transport parameter:



##############################################################################
### RUN CONTROL
##############################################################################

:start run control:                                                                                                                                        
    ncase = 1000000
:stop run control:

:start rng definition:
    initial seeds = 291 3871  
:stop rng definition:



##############################################################################
### PARTICLE TRACKS
##############################################################################
:start ausgab object definition:
    :start ausgab object:
        name    = my_tracks          
        library = egs_track_scoring  
        stop scoring = 10000
    :stop ausgab object:
:stop ausgab object definition:



##############################################################################
### VIEWER CONTROL
##############################################################################

:start view control:
    xmin = -10
    xmax =  10
    ymin = -10
    ymax =  10
    zmin = -10
    zmax =  10
:stop view control:
