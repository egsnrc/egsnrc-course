#!/usr/bin/python

# Coordinate transformation from DICOM to DOSXYZnrc
#    Input:  gantry angle, couch angle and collimator angle from DICOM
#    Output: theta, phi, phicol for DOSXYZnrc (using BEAMnrc phsp)
#
# This code is based on the example provided in the article:
#
# Beam coordinate transformations from DICOM to DOSXYZnrc
# Lixin Zhan, Runqing Jiang and Ernest K Osei
# Published 23 November 2012 Institute of Physics and Engineering in Medicine
# Physics in Medicine & Biology, Volume 57, Number 24
#

import sys
import math

def dcm2dosxyz(AngleGantry, AngleCouch, AngleCollimator):

    gamma = AngleGantry*math.pi/180.0
    col   = AngleCollimator*math.pi/180.0
    rho   = AngleCouch*math.pi/180

    # Distort couch and gantry angles slightly to avoid special cases
    if AngleCouch in (90.0,270.0) and AngleGantry in (90.0,270.0):
        rho   = rho   * 0.999999
        gamma = gamma * 0.999999

    # Do calculations
    sgsr = math.sin(gamma)*math.sin(rho)
    sgcr = math.sin(gamma)*math.cos(rho)
    theta = math.acos(-sgsr)
    phi = math.atan2(-math.cos(gamma),sgcr)
    CouchAngle2CollPlane = math.atan2(-math.sin(rho)*math.cos(gamma), math.cos(rho))
    phicol = (col-math.pi/2) + CouchAngle2CollPlane

    # Coord transformation for BEAMnrc generated phsp to DOSXYZnrc
    phicol = math.pi - phicol

    return (theta*180/math.pi, phi*180/math.pi % 360, phicol*180/math.pi % 360)


def main(argv):
    if len(argv) != 3:
        print "Usage: coordinateTransform.py gantryAngle couchAngle collimatorAngle"
        return

    print("Gantry, couch, collimator:",float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]))
    [theta, phi, phicol] = dcm2dosxyz(float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]))
    print("Theta, phi, phicol:", theta, phi, phicol)

if __name__ == "__main__":
   main(sys.argv[1:])
